题目四：如何检查错别字
如何设计一个方案去检测中文错别字。请给出具体的方案，包括数据准备，模型选择和测评方法等部分。
据自己所知，存在现有的 pycorrector 包  可以做错别字的检测和纠错工作。调用方式如下：
答：
import pycorrector

corrected_sent, detail = pycorrector.correct('少先队员因该为老人让坐')
print(corrected_sent, detail)

如实是方案，一般分如下几部：
1.	准备预料数据
训练语料<人民日报2014版熟语料>，包括： 1）标准人工切词及词性数据people2014.tar.gz， 2）未切词文本数据people2014_words.txt， 3）kenlm训练字粒度语言模型文件及其二进制文件people2014corpus_chars.arps/klm， 4）kenlm词粒度语言模型文件及其二进制文件people2014corpus_words.arps/klm
网盘链接:https://pan.baidu.com/s/1971a5XLQsIpL0zL0zxuK2A 密码:uc11。
2.	分词，根据词表合成词序列
3.	根据语言模型定位错误词和错误字，可选择语言模型如下

kenlm: kenlm统计语言模型工具
rnn_lm: TensorFlow、PaddlePaddle均有实现栈式双向LSTM的语言模型
rnn_attention模型: 参考Stanford
University的nlc模型，该模型是参加2014英文文本纠错比赛并取得第一名的方法
rnn_crf模型: 参考阿里巴巴2016参赛中文语法纠错比赛并取得第一名的方法
seq2seq模型: 使用序列模型解决文本纠错任务，文本语法纠错任务中常用模型之一
seq2seq_attention模型: 在seq2seq模型加上attention机制，对于长文本效果更好，模型更容易收敛，但容易过拟合

阅题人您好，
我是应聘数据挖掘岗位的求职者，很荣幸能有机会进行第一轮的笔试。
但是当我看到这份考试的时候我发现很是尴尬，因为先前的工作基本是数据挖掘的数据建模方向，
曾经虽然曾经做过NER的工作，但对文本识别类的工作涉足并不深，写正则也是基础。
不过还是想进入贵公司的团队，好好沉淀，散发我的光和热。所以硬着头皮做了题目，我有具体了解过某些技术，但时间有限，没有做深入研究。
希望能给个机会进入面试环节，考察我的综合能力。让我能展现自己的优势。
谢谢。

a. 平时阅读哪些技术类书籍
美团机器学习实践
深度学习
b. 当遇到技术问题时，常去哪些网站寻找帮助
CSDN
github
博客园
stake overflower
机器之心
知乎
