题目二： 计算 TF-IDF
1. 给你一个文本数据集，请实现 TF-IDF 的计算方法。
2. 假如文本数据量很大无法全量载入到单机内存完成，如何利用多台机器实现分布式的操作，请用 map-reduce 或 MPI 框架完成。
答：
1.
from sklearn.feature_extraction.text import TfidfTransformer  
from sklearn.feature_extraction.text import CountVectorizer  

corpus=[Text]

vectorizer=CountVectorizer()

transformer = TfidfTransformer()
tfidf = transformer.fit_transform(vectorizer.fit_transform(corpus))  
print (tfidf)   
2.使用pyspark进行分布式计算
from pyspark.sql import SQLContext
from pyspark import SparkContext
from pyspark.ml.feature import HashingTF, IDF
from pyspark.ml import Pipeline
sc =SparkContext()
sqlContext = SQLContext(sc)
data = sqlContext.read.format('com.databricks.spark.csv').options(header='true', 
inferschema='true').load('text.txt')

hashingTF = HashingTF(inputCol="filtered", outputCol="rawFeatures", numFeatures=10000)
idf = IDF(inputCol="rawFeatures", outputCol="features", minDocFreq=5) 
pipeline = Pipeline(stages=[ hashingTF, idf])
pipelineFit = pipeline.fit(data)
dataset = pipelineFit.transform(data)
TF_IDF=dataset['hashingTF']*dataset['idf']
