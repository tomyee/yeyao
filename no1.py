#题目一：
##请用 python 实现如下函数：
#def generate(text : str, map : Dict[str, str]):
    #pass
#答：
import re
a_dict = {'a':['B','C'],'b':'X',}
content = 'adcbf'
def generate(text,map_):
    new=sorted(map_.items(),key = lambda x:len(x[1]),reverse = False)
    new_dic=dict(new)
    for keys in new_dic:
        strinfo = re.compile(keys)
        for i in new_dic.get(keys):        
            text = strinfo.sub(i,text)
    return text
a=generate(content,a_dict)
print(a)
